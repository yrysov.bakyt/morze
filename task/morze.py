def code_morze(input):
    morse_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.',
         'D': '-..', 'E': '.', 'F': '..-.', 
         'G': '--.', 'H': '....', 'I': '..', 
         'J': '.---', 'K': '-.-', 'L': '.-..',
         'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 
         'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--',
         'Z': '--..', '1': '.----', '2': '..---', '3': '...--',
         '4': '....-', '5': '.....', '6': '-....', '7': '--...', 
         '8': '---..', '9': '----.', '0': '-----',
         ', ': '--..--', '.': '.-.-.-', '?': '..--..',
         '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'
    }

    input = input.upper()
    result = []

    for i in input:
        if i in morse_dict:
            result.append(morse_dict[i])
    
    return ' '.join(result)
